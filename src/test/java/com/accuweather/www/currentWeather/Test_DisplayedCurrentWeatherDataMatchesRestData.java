package com.accuweather.www.currentWeather;

import org.testng.annotations.Test;

import com.accuweather.Environment;
import com.accuweather.api.helpers.CurrentConditionsHelper;
import com.accuweather.api.objects.Condition;
import com.accuweather.www.pages.CurrentWeatherPage;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.utils.TestReporter;

public class Test_DisplayedCurrentWeatherDataMatchesRestData extends WebBaseTest {
    private CurrentWeatherPage currentWeather = new CurrentWeatherPage();

    @Test
    public void test_DisplayedCurrentWeatherDataMatchesRestData() {
        Condition condition = CurrentConditionsHelper.getUSCityCurrentCondition(Environment.getEnvironment(), "New York", "NY");
        setPageURL(condition.getLink());
        testStart("test_DisplayedCurrentWeatherDataMatchesRestData");
        currentWeather.validateCurrentConditions(condition);
        TestReporter.assertAll();
    }
}
