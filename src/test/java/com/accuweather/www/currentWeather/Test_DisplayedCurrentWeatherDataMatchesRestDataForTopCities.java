package com.accuweather.www.currentWeather;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.accuweather.Environment;
import com.accuweather.api.AccuweatherRest;
import com.accuweather.api.helpers.CurrentConditionsHelper;
import com.accuweather.api.objects.Condition;
import com.accuweather.api.objects.Location;
import com.accuweather.www.pages.CurrentWeatherPage;
import com.chameleon.api.restServices.RestResponse;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.utils.TestReporter;

public class Test_DisplayedCurrentWeatherDataMatchesRestDataForTopCities extends WebBaseTest {
    private CurrentWeatherPage currentWeather = new CurrentWeatherPage();

    @DataProvider(name = "topcities", parallel = true)
    public Location[][] topcities() {
        RestResponse rest = AccuweatherRest.locations().v1(Environment.PROD).topCities("50", false);
        Location[] locations = rest.mapJSONToObject(Location[].class);
        Location[][] location = new Location[50][];
        for (int x = 0; x < 50; x++) {
            Location[] obj = new Location[] { locations[x] };
            location[x] = obj;
        }
        return location;
    }

    @Test(dataProvider = "topcities")
    public void test_DisplayedCurrentWeatherDataMatchesRestDataForTopCities(Location location) {
        TestReporter.setDebugLevel(0);
        Condition condition = CurrentConditionsHelper.getCityCurrentCondition(Environment.PROD, location.getCountry().getID(), location.getEnglishName(), location.getAdministrativeArea().getID());
        setPageURL(condition.getLink());
        testStart("test_DisplayedCurrentWeatherDataMatchesRestData");

        currentWeather.validateCurrentConditions(condition);
        TestReporter.assertAll();

    }
}
