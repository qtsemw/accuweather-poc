package com.accuweather.www.navigation;

import org.testng.annotations.Test;

import com.accuweather.www.AccuWeatherBaseTest;
import com.accuweather.www.WebRoutes;
import com.accuweather.www.pages.NavigationBar;

public class Test_CanNavigateToWeatherNews extends AccuWeatherBaseTest {
    private NavigationBar navBar = new NavigationBar();

    @Test
    public void test_CanNavigateToWeatherNews() {
        testStart("CanNavigateToWeatherNews");
        navBar.navigateToNews();
        validateCurrentURL(WebRoutes.WEATHER_NEWS, getCurrentLanguage());
    }

}
