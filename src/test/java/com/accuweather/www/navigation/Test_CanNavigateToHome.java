package com.accuweather.www.navigation;

import org.testng.annotations.Test;

import com.accuweather.www.AccuWeatherBaseTest;
import com.accuweather.www.WebRoutes;
import com.accuweather.www.pages.NavigationBar;

public class Test_CanNavigateToHome extends AccuWeatherBaseTest {
    private NavigationBar navBar = new NavigationBar();

    @Test
    public void test_CanNavigateToRadarAndMaps() {
        testStart("CanNavigateToRadarAndMaps");
        navBar.navigateToSevereWeather();
        validateCurrentURL(WebRoutes.SEVERE_WEATHER, getCurrentLanguage(), "us");

        navBar.navigateToHome();
        validateCurrentURL(WebRoutes.MAIN);
    }

}
