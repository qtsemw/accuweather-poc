package com.accuweather.www.navigation;

import org.testng.annotations.Test;

import com.accuweather.LocationData;
import com.accuweather.www.AccuWeatherBaseTest;
import com.accuweather.www.WebRoutes;
import com.accuweather.www.pages.NavigationBar;

public class Test_CanNavigateToRadarAndMaps extends AccuWeatherBaseTest {
    private NavigationBar navBar = new NavigationBar();

    @Test
    public void test_CanNavigateToRadarAndMaps() {
        testStart("CanNavigateToRadarAndMaps");
        navBar.navigateToRadarAndMaps();
        setGeoLocation(LocationData.HIGH_POINT_NC);
        validateCurrentURL(WebRoutes.WEATHER_RADAR, getCurrentLanguage(), "us", "north-carolina");
    }

}
