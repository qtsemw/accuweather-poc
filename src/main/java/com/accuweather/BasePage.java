package com.accuweather;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang3.math.NumberUtils;
import org.openqa.selenium.By;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebPageLoaded;
import com.chameleon.selenium.web.elements.WebElement;
import com.chameleon.selenium.web.elements.WebTextbox;
import com.chameleon.utils.TestReporter;

public class BasePage {
    protected ExtendedWebDriver getDriver() {
        return DriverManager.getWebDriver();
    }

    protected void clickVisibleElement(final By by) {
        WebPageLoaded.isDomInteractive();
        WebElement element = getDriver().findElement(by);
        element.syncVisible(15);
        element.click();
    }

    protected void enterTextVisibleElement(final By by, final String text) {
        WebPageLoaded.isDomInteractive();
        final WebTextbox element = getDriver().findTextbox(by);
        element.syncVisible();
        element.set(text);
    }

    protected void validate(final By by, final String elementName, final Object expectedText) {
        final WebElement element = getDriver().findElement(by);
        final String elementText = element.getText();
        final boolean found = elementText.contains(expectedText.toString());
        TestReporter.softAssertTrue(found, "Validate element [ " + elementName + " ] contains text [ " + expectedText + " ]. Found text [ " + elementText + " ]");
        if (!found) {
            element.highlight();
            // TestReporter.logScreenshot(element, elementName);
        }
    }

    protected String round(Object obj, int roundingPrecision) {
        if (NumberUtils.isCreatable(obj.toString())) {
            BigDecimal bd = new BigDecimal(obj.toString());
            bd = bd.setScale(roundingPrecision, RoundingMode.HALF_UP);
            return bd.toString();
        }
        return obj.toString();
    }
}
