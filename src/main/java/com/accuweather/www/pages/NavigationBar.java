package com.accuweather.www.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.accuweather.BasePage;
import com.chameleon.selenium.web.elements.WebTextbox;
import com.chameleon.utils.Constants;

public class NavigationBar extends BasePage {
    private By byLogo = By.xpath("//div[@id='header-logo']");
    private By byRadarMaps = By.id("navRadar");
    private By byNews = By.id("navNews");
    private By byVideo = By.id("navVideo");
    private By bySevereWeather = By.id("navSevere");
    private By byMore = By.id("navMore");
    private By byStartSearch = By.className("search-input");
    private By bySuperSearch = By.className("super-search-input");

    public void navigateToHome() {
        clickVisibleElement(byLogo);
    }

    public void navigateToRadarAndMaps() {
        clickVisibleElement(byRadarMaps);
    }

    public void navigateToNews() {
        clickVisibleElement(byNews);
    }

    public void navigateToVideo() {
        clickVisibleElement(byVideo);
    }

    public void navigateToSevereWeather() {
        clickVisibleElement(bySevereWeather);
    }

    public void locationSearch(String info) {
        WebTextbox txtSearch = getDriver().findTextbox(byStartSearch);
        txtSearch.syncEnabled();
        txtSearch.click();
        WebTextbox txtSuperSearch = getDriver().findTextbox(bySuperSearch);
        txtSuperSearch.syncVisible();
        txtSuperSearch.set(info);
        txtSuperSearch.sendKeys(Keys.RETURN);
    }

    public boolean validateNavigation(String url) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Constants.PAGE_TIMEOUT);
        return wait.until(ExpectedConditions.urlContains(url));
    }
}
