package com.accuweather.api.currentconditions;

import com.accuweather.Environment;
import com.accuweather.api.BaseAPI;
import com.accuweather.api.currentconditions.v1.V1;

public class CurrentConditions extends BaseAPI {
    private final static String PATH = "currentconditions";

    public V1 v1() {
        return new V1(getRootURL(Environment.getEnvironment()) + PATH);
    }

    public V1 v1(final String environment) {
        return new V1(getRootURL(environment) + PATH);
    }
}
