package com.accuweather.api;

import com.accuweather.api.currentconditions.CurrentConditions;
import com.accuweather.api.locations.Locations;

public class AccuweatherRest {

    public static CurrentConditions currentConditions() {
        return new CurrentConditions();
    }

    public static Locations locations() {
        return new Locations();
    }
}
